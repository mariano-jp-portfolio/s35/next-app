export default [
    {
        id: "wdc001",
        name: "HTML",
        description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        price: 45000,
        onOffer: true,
        start_date: 'Feb 20, 2021',
        end_date: 'Aug 19, 2021'
    },
    {
        id:"wdc002",
        name:"Phyton Django",
        description: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
        price: 55000,
        onOffer: true,
        start_date: 'Jun 19, 2021',
        end_date:'Sep 18, 2021',
    },
    {
        id: "wdc003",
        name: "React JS",
        description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        price: 62000,
        onOffer: true,
        start_date: 'Jul 20, 2021',
        end_date: 'Oct 4, 2021'
    }
]
import { useState, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import UserContext from '../../UserContext';
// import usersData from '../../data/usersdata';
import Router from 'next/router';



export default function index() {
	// const [user, setUser] = useState('');
	const { setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	function authenticate(e) {

		//prevent redirection via form submission
		e.preventDefault();
		
		// fetch().then().then()
		// fetch requires the endpoint (url) & an object {method, headers, body}
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			// if successful login, a token will be generated, save it in the user's localStorage
			localStorage.setItem('token', data.accessToken);
			if (data !== null) {				
				// send another fetch request to decode this token and redirect or route the user to courses page
				fetch('http://localhost:4000/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data);
					localStorage.setItem('id', data._id);
					localStorage.setItem('isAdmin', data.isAdmin);
					
					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					});
				});
				
				setEmail('');
		    	setPassword('');
				
				// welcome alert
				alert(`Welcome back, ${email}`)
				
				// router
				Router.push('/courses');
			} else {
				alert('Authentication failed');
			}
		});

		// if conditions are met will return true
		// if conditions are not met will return false
		// also returns the object that meets the condition
		// const match = usersData.find(user => {
		//     return (user.email === email && user.password === password);
		// })

		// if(match){
		//     localStorage.setItem('email', email);
		//     localStorage.setItem('isAdmin', match.isAdmin);

		//     setUser({
		//         // email: localStorage.getItem('email'),
		//         // isAdmin: localStorage.getItem('isAdmin')
		//         email: match.email,
		//         isAdmin: match.isAdmin
		//     });

		//     Router.push('/courses');
  //       } else {
		//     console.log("Authentication failed, no match found.")
		// 	}
	}

	return (
		<Container>
			<h2>Login</h2>
		    <Form onSubmit={e => authenticate(e)}>
		        <Form.Group controlId="userEmail">
	                <Form.Label>Email address</Form.Label>
	                <Form.Control 
	                    type="email" 
	                    placeholder="Enter email" 
	                    value={email}
	                    onChange={(e) => setEmail(e.target.value)}
	                    required
	                />
		        </Form.Group>

		        <Form.Group controlId="password">
		            <Form.Label>Password</Form.Label>
			        <Form.Control 
	                    type="password"
	                    placeholder="Password" 
			            value={password}
			            onChange={(e) => setPassword(e.target.value)}
			            required
			        />
			    </Form.Group>

			    <Button className="bg-primary" type="submit">
			            Submit
			    </Button>
			</Form>
		</Container>
	)
}


import React, { useContext } from 'react';
import coursesData from '../../data/coursesdata';
import CourseCard from '../../components/CourseCard';
import UserContext  from '../../UserContext';
import Head from 'next/head'

import { Table, Button } from 'react-bootstrap';
import coursesdata from '../../data/coursesdata';

export default function index(){
    const { user } = useContext(UserContext);
    // courseData will store the active courses
    let courseData;

    const courses = coursesData.map(indivCourse => {
        return (
            <CourseCard
                key={indivCourse.id}
                courseProp={indivCourse}
            />
        );
    })
    
    // fetch('http://localhost:4000/api/courses') //getAll
    // .then(res => res.json())
    // .then(data => {
    //     if (data.length < 1) {
    //         courseData = 'No courses available.'
    //     } else if (user.isAdmin === 'false') {
    //         // for regular user
    //         courseData = data.map(course => {
    //             return (
    //                 <CourseCard 
    //                     key={course.id}
    //                     courseProp={course}
    //                 />    
    //             );
    //         }) 
    //     } else {
    //         // for admin
    //         courseData = data.map(course => {
    //             return (
    //                 <tr key={course._id}>
    //                     <td>{course.name}</td>
    //                     <td>{course.description}</td>
    //                     <td>{course.price}</td>
    //                     <td>{course.isActive ? 'Open' : 'Closed'}</td>
    //                     <td>{course.createdOn}</td>
    //                     <td>
    //                         <Button variant="success">Update</Button>
    //                         <Button id="disableCourseBtn" variant="danger">Disable</Button>
    //                     </td>
    //                 </tr>
    //             );
    //         })
    //     }
    // })

    const courseRow = coursesData.map(indivCourse => {
        return(
            <tr key={indivCourse.id}>
                <td>{indivCourse.id}</td>
                <td>{indivCourse.name}</td>
                <td>{indivCourse.description}</td>
                <td>{indivCourse.price}</td>
                <td>{indivCourse.onOffer ? 'open': 'closed'}</td>
                <td>{indivCourse.start_date}</td>
                <td>{indivCourse.end_date}</td>
                <td>
                    <Button variant="success">Update</Button>
                    <Button id="disableCourseBtn" variant="danger">Disable</Button>
                </td>
            </tr>

        )
    })

    return (
            user.isAdmin === true
            ?
            <React.Fragment>
                <Head>
		                <title>Courses Admin Dashboard</title>
		        </Head>

                <h1>Course Dashboard</h1>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Created On</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        { courseRow }
                    </tbody>
                </Table>
            </React.Fragment>
            :
            <React.Fragment>
                { courses }
            </React.Fragment>
              
    )
}